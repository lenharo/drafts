# Banner For Debian 28th Anniversary

This banner was created by me (Daniel Lenharo de Souza) with a contribution from Valessio Brito to celebrate 28 years of the Debian Project.



## License
[MIT](https://choosealicense.com/licenses/mit/)
